FROM ubuntu:22.04
# --build-arg VERIFICATION_TOKEN --build-arg ENDPOINT
RUN apt-get update && apt-get install -y curl build-essential ca-certificates
RUN update-ca-certificates
COPY target/debug/ebay-db target/debug/
EXPOSE 8080
ARG MONGODB_URI
ENV MONGODB_URI=$MONGODB_URI
ARG AMQP_ADDR
ENV AMQP_ADDR=$AMQP_ADDR
ENV RUST_LOG=info
CMD target/debug/ebay-db