use ebay_schema::test::TestMessage;
use futures_lite::stream::StreamExt;
use lapin::message::{Delivery};
use lapin::options::{
    BasicAckOptions, BasicConsumeOptions, QueueDeclareOptions,
};
use lapin::types::FieldTable;
use lapin::{Connection, ConnectionProperties};
use mongodb::{
    error::Result, options::ClientOptions, results::InsertOneResult, Client,
    Collection, Database,
};
use std::time::Duration;
use std::{env, sync::Arc};

#[tokio::main()]
async fn main() -> Result<()> {
    tracing_subscriber::fmt::init();

    // mongodb
    let client_uri = env::var("MONGODB_URI")
        .expect("You must set the MONGODB_URI environment var!");
    tracing::info!("mongodb uri {}", &client_uri);
    let mut client_options = ClientOptions::parse(&client_uri).await?;
    client_options.heartbeat_freq = Some(Duration::from_secs(60 * 5));
    let client = Client::with_options(client_options)?;
    let database = client.database("ebay");
    let shared_database = Arc::new(database);

    // rabbitmq
    let amqp_addr = std::env::var("AMQP_ADDR").unwrap_or_else(|_| {
        "amqp://rabbitmq-1668544038.rabbitmq.svc.cluster.local:5672/vhe".into()
    });
    let conn_props = ConnectionProperties::default()
        .with_executor(tokio_executor_trait::Tokio::current())
        .with_reactor(tokio_reactor_trait::Tokio);
    let amqp_conn = Connection::connect(&amqp_addr, conn_props)
        .await
        .unwrap();
    let amqp_chan = amqp_conn
        .create_channel()
        .await
        .unwrap();
    let mut queue_opt = QueueDeclareOptions::default();
    queue_opt.durable = true;
    let _amqp_queue = amqp_chan
        .queue_declare(
            "ebay",
            queue_opt,
            FieldTable::default(),
        )
        .await
        .unwrap();
    let mut amqp_consumer = amqp_chan
        .basic_consume(
            "ebay",
            "ebay",
            BasicConsumeOptions::default(),
            FieldTable::default(),
        )
        .await
        .unwrap();

    while let Some(delivery) = amqp_consumer
        .next()
        .await
    {
        let delivery = delivery.expect("error in consumer");
        amqp_delivered(delivery, shared_database.clone()).await
    }
    Ok(())
}

async fn amqp_delivered(delivery: Delivery, database: Arc<Database>) {
    let test_collection: Collection<TestMessage> = database.collection("test");
    let test_message: TestMessage =
        serde_json::from_slice(&delivery.data).unwrap();
    let insert_result: Result<InsertOneResult> = test_collection
        .insert_one(test_message, None)
        .await;
    tracing::info!("insert {:#?}", insert_result);
    match insert_result {
        Ok(_result) => delivery
            .ack(BasicAckOptions::default())
            .await
            .expect("Failed to ack message"),
        Err(err) => tracing::info!("insert err {:#?}", err),
    }
}
